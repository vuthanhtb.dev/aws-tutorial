#Assume role cli:
aws sts assume-role --role-arn <role-arn> --role-session-name <session-name> 

#Example
aws sts assume-role --role-arn arn:aws:iam::686386735812:role/test-poweruser-role --role-session-name test-assume 
